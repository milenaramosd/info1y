/*
Siano date 3 formule matematiche:
P(a) = a^2 - a^3 + sin(a)
R(b) = radicequadrata(b^2 + 12)
S(x, y) = 13x + (x + y)/3

Scrivere un programma che accetti due numeri n e m e calcoli il valore dell'espressione:

S(P(n), R(n-m))

e lo stampi a video
*/

#include <stdio.h>
#include <math.h>

int main()
{

    double n, m, P, R, S;

    printf("Inserisci il primo valore: ");
    scanf("%lf%*c", &n);
    printf("Inserisci il secondo valore: ");
    scanf("%lf%*c", &m);

    P = pow(n,2) - pow(n,3) + sin(n);
    R = sqrt(pow((n-m),2) + 12); //pow(x,y) eleva x alla y
    S = 13*P +(P+R)/3;

    printf("Il valore dell'espressione è: %lf\n", S);

    return 0;
}
