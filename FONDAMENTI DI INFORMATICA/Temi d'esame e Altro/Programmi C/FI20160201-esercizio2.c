/*

Si consideri una lista dinamica di interi definita come:

struct nodo
{
    int val;
    struct nodo *next;
};

typedef struct nodo *lista;

Implementare una funzione somma che riceve in ingresso due liste di interi a e b, e restituisce una nuova lista di interi c, i cui elementi sono la somma
degli elementi di a e di b in posizione corrispondente. Nel caso a e b, non abbiano la stessa lunghezza, gli elementi in eccesso (nella lista a o nella lista b)
verranno copiati in c senza essere sommati a nulla.

Dichiarare il prototipo della funzione somma nel modo che si ritiene più opportuno e fornirne una implementazione, utilizzando (se necessario) la seguente
funzione (di cui non è necessario fornire un’implementazione):

void inserisci (lista *l, int val)

che aggiunge in coda alla alla lista  l un elemento di valore val.

Esempio
Siano a: 3 -> 6 -> 8 e b: 4 -> 7 -> 4 -> 11 -> 15
somma (a,b) sarà c: 7 -> 13 -> 12 -> 11 -> 15


*/

#include <stdio.h>
#include <stdlib.h>

lista somma(lista *a, lista *b) {
    lista somma = NULL;
    while(a!=NULL && b!=NULL) {
        inserisci(&somma, a->val+b->val);
        a = a->next;
        b = b->next;

        if(b!=NULL) {
            inserisci(&somma, b->val);
            b = b->next;
        }
    }

    if(a!=NULL) {
        while(a!=NULL) {
            inserisci(&somma, a->val);
            a = a->next;
        }
    } else {
        while(b!=NULL) {
            inserisci(&somma, b->val);
            b = b->next;
        }
    }


    return somma;
}
