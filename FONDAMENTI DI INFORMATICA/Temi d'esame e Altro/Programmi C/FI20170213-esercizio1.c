/*
La società Juice Inc., per analizzare la produttività dei suoi 100 programmatori, ha  raccolto  per  ciascuno  di  loro  le seguenti informazioni:
nome, cognome, età, le ore svolte e il numero di righe (in migliaia) di codice scritte (settimanali) nelle ultime 10 settimane.

Un esempio dei dati raccolti per un dipendente è il seguente:

•	Nome: Mario
•	Cognome: Rossi
•	Età: 35
•	Ore settimanali nelle ultime 10 settimane: 40,40,42,43,45,46,50,38,40,40
•	Righe di codice settimanali nelle ultime 10 settimane: 3.1, 3, 3.2, 3.3, 3.5, 3.1, 2.9, 2.7, 3.6, 3.3

A.	Dichiarare un tipo di dati  programmatore in C che contenga tutte le informazioni raccolte dalla Juice Inc. per ciascuno dei suoi programmatori.
B.	Completare il seguente frammento di codice C in modo che vengano copiati nell’array selezione i dati dei programmatori che (i) hanno meno di 30 anni e (ii)
nelle ultime 10 settimane, hanno prodotto un numero medio di righe di codice superiore o uguale al numero medio di righe di codice prodotte da tutti i programmatori
della Juice Inc.; infine il frammento di codice deve anche stampare a video il numero di programmatori nella variabile selezione:

programmatore database[100], selezione[100];

carica(database,100); //carica in database tutti i dati dei programmatori

// inserire qui il frammento di codice

Note. La funzione carica riempe interamente la variabile database con i dati di tutti e 100 i programmatori; si ipotizzi inoltre che per ogni programmatore siano
disponibili i dati di tutte le 10 settimane precedenti.

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef struct {
    char nome[30];
    char cognome[30];
    int eta;
    int ore[10];
    float righe[10]
} programmatore;

//....

    programmatore database[100], selezione[100];

    carica(database,100); //carica in database tutti i dati dei programmatori

    float mediaGlobale=0, media;
    int n = 0;

    for(int i=0; i<100; i++)
        for(int j=0, j<10; j++)
            mediaGlobale += database[i].righe[j];

    mediaGlobale = mediaGlobale / (10.0*100.0);

    for(int i=0; i<100; i++) {
        media = 0.0;
        for(int j=0, j<10; j++)
            media += database[i].righe[j];

        media = media / 10.0;
        if(database[i].eta < 30 && media >= mediaGlobale) {
            selezione[i] = database[i];
            n++;
        }
    }

    printf("Sono stati trovati %d sviluppatori che soddisfano i requisiti\n", n);
