/*
Implementare una funzione, stampa_distinti, che riceve come parametri in ingresso una stringa nomefile, contenente il nome di un file testuale, ed un numero intero n.
La funzione ha il compito di aprire il file specificato attraverso il parametro nomefile e stampare a video i primi n caratteri distinti (cioè diversi fra loro)
separati da uno spazio.

Note. Il file può contenere qualsiasi carattere ASCII.

Esempio
Si supponga che il file nomefile abbia questo contenuto: Ad esempio, abbiamo questo contenuto. La chiamata stampa_distinti(nomefile, 12)
stamperà a video i seguenti caratteri :

A	d		e	s	m	p	i	o	,	a	b

*/

/* bug: FUNZIONA SOLO CON NUMERO n FISSO POICHÈ NON POSSO INIZIALIZZARE IN MODO VARIABILE L'ARRAY */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void stampa_distinti(char nomefile[], int n);
int cerca_elemento(int array[], int n, char c);

int main() {
    stampa_distinti("FI20170705-esercizio1.txt", 10);
    return 0;
}

void stampa_distinti(char nomefile[], int n) {
    FILE *f;
    f = fopen(nomefile, "r");
    int distinti[10] = {0};
    int i = 0;
    char c;

    while((c = fgetc(f)) != EOF && i<n) {
        if(cerca_elemento(distinti, n, c) == 0) {
            distinti[i] = c;
            i++;
        }
    }

    i;

    for(int k=0; k<i; k++)
        printf("%c ", distinti[k]);
}

int cerca_elemento(int array[], int n, char c) {
    int i, trovato = 0;
    for(i=0; i<n; i++)
        if(array[i] == c) {
            trovato = 1;
            i=n;
    }
    return trovato;
}
