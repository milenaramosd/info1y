//

#include <stdio.h>

int fattric(int num){
    
    if (num==0 || num==1)
        
        return 1;
    
    else
        
        return num*(fattric(num-1));
    
}

int main() {
    
    int num;
    
    printf("Inserire un numero e ne calcolo il fattoriale: ");
    
    scanf("%d", &num);
    
    if(num<0){
        
        printf("Inserire un numero positivo!\n");
    
    return 0;
        
    }

    else{
        
        printf("Il fattoriale di %d e': %d\n", num, fattric(num));

    return 0;
        
    }

}
