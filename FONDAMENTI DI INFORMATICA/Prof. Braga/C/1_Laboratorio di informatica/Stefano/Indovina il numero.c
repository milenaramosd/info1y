#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX 100

int main(){
	
	int n=0, guess=0, stop=1;
	srand(time(NULL));
	n=rand()%(MAX+1);
	printf("Ho pensato ad un numero compreso tra 0 e %d. Indovina qual e': ", MAX);
	scanf("%d", &guess);
	while(stop!=0){
		if(guess>n){
			printf("No, e' piu' alto del mio\n\nRiprova: ");
		}
		else if(guess<n){
			printf("No, e' piu' basso del mio\n\nRiprova: ");
		}
		else if(guess=n){
			printf("Bravo hai indovinato!!");
			stop=0;
		}
		scanf("%d", &guess);
	}
	
	
	return 0;
	
}
