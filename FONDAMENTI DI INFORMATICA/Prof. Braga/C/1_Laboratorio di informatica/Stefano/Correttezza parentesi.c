#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


typedef char TipoElemento;

typedef struct EL{
	TipoElemento info;
	struct EL * prox;
} ElemLista;

typedef ElemLista * ListaDiElem;

int verificapresenzaintesta(ListaDiElem lista, TipoElemento elem);
ListaDiElem insinordineric(ListaDiElem lista, TipoElemento elem);
int verificapresenzaric(ListaDiElem lista, TipoElemento elem);
void visualizzalistaric(ListaDiElem lista);
void distruggilistaric(ListaDiElem lista);
ListaDiElem insintesta(ListaDiElem lista, TipoElemento elem);
ListaDiElem invertilista(ListaDiElem lista);
ListaDiElem cancellaunoric(ListaDiElem lista, TipoElemento elem);
ListaDiElem insincodaric(ListaDiElem lista, TipoElemento elem);
int contaelem(ListaDiElem lista);

int main(){
	
	FILE * file;
	ListaDiElem pila=NULL;
	char c;
	printf("---Ordine delle parentesi---\n");
	if(!(file=fopen("testo.txt", "r"))){
		printf("Errore nell'apertura del file");
		return 0;
	}
	fscanf(file, "%c", &c);
	for( ; (c!='\n') || !feof(file) ; ){
		if(c=='{' || c=='[' || c=='('){
			pila=insintesta(pila, c);
		}
		else if(c=='}'){
			if(verificapresenzaintesta(pila, '{')){
				pila=cancellaunoric(pila, '{');
			}
			else{
				printf("C'e' un errore di sintassi");
				return 0;
			}
		}
		else if(c==']'){
			if(verificapresenzaintesta(pila, '[')){
				pila=cancellaunoric(pila, '[');
			}
			else{
				printf("C'e' un errore di sintassi");
				return 0;
			}
		}
		else if(c==')'){
			if(verificapresenzaintesta(pila, '(')){
				pila=cancellaunoric(pila, '(');
			}
			else{
				printf("C'e' un errore di sintassi");
				return 0;
			}
		}
		fscanf(file, "%c", &c);
	}
	if(pila==NULL){
		printf("Sintassi corretta");
	}
	else{
		printf("Sono rimaste delle parentesi aperte");
	}
	fclose(file);
	
	return 0;
}

int verificapresenzaintesta(ListaDiElem lista, TipoElemento elem){
	if(lista!=NULL && lista->info==elem){
		return 1;
	}
	return 0;
}

ListaDiElem cancellaunoric(ListaDiElem lista, TipoElemento elem){
	ListaDiElem temp;
	if(lista==NULL){
		return lista;
	}
	if(lista->info!=elem){
		lista->prox=cancellaunoric(lista->prox, elem);
		return lista;
	}
	else if(lista->info==elem){
		temp=lista->prox;
		free(lista);
		return temp;
	}
}

ListaDiElem insinordineric(ListaDiElem lista, TipoElemento elem){
	ListaDiElem temp;
	if(lista==NULL){
		temp=malloc(sizeof(ElemLista));
		temp->info=elem;
		temp->prox=NULL;
		return temp;
	}
	if(lista->info<elem){
		temp=malloc(sizeof(ElemLista));
		temp->info=elem;
		temp->prox=lista;
		return temp;
	}
	if(lista->info>elem){
		lista->prox=insinordineric(lista->prox, elem);
	}
	return lista;
}

ListaDiElem insincodaric(ListaDiElem lista, TipoElemento elem){
	if(lista==NULL){
		lista=malloc(sizeof(ElemLista));
		lista->info=elem;
		lista->prox=NULL;
		return lista;
	}
	else{
		lista->prox=insincodaric(lista->prox, elem);
	}
	return lista;
}

ListaDiElem insintesta(ListaDiElem lista, TipoElemento elem){
	ListaDiElem temp;
	temp=malloc(sizeof(ElemLista));
	temp->info=elem;
	temp->prox=lista;
	return temp;
}

void distruggilistaric(ListaDiElem lista){
	if(lista!=NULL){
		distruggilistaric(lista->prox);
		free(lista);
	}
}

int verificapresenzaric(ListaDiElem lista, TipoElemento elem){
	if(lista==NULL){
		return 0;
	}
	if(lista->info==elem){
		return 1;
	}
	return verificapresenzaric(lista->prox, elem);
}

void visualizzalistaric(ListaDiElem lista){
	ListaDiElem temp;
	if(lista==NULL){
		printf("--||\n");
	}
	else{
		printf("%d", lista->info);
		if(lista->prox!=NULL){
			printf("-->");
		}
		visualizzalistaric(lista->prox);
	}
}
