#include <stdio.h>

int main (){
	
	int sen=0, sum=0, num=0, max=0, min=0, pari=0, dispari=0;
	float sumf=0, numf=0;
	
	printf("----MAX, MIN, MEDIA, N.PARI, N.DISPARI----\n");
	printf("Scrivi uno o piu' numeri interi positivi o inserisci 0 per terminare la sequenza: ");
	scanf("%d", &sen);
	
	if(sen==0)
		printf("Non hai inserito nessun numero");
	else{
		max=sen;
		min=sen;
		
		while(sen!=0){
			num=num+1;
			sum=sum+sen;
			
			if(sen>max)
				max=sen;
			if(sen<min)
				min=sen;
			if(sen%2==0)
				pari=pari+1;
			else
				dispari=dispari+1;
			scanf("%d", &sen);
		}
	}
	sumf=sum;
	numf=num;
	if(num!=0)
		printf("\npari: %d\ndispari: %d\nminimo = %d\nmassimo = %d\nmedia = %f", pari, dispari, min, max, sumf/numf);
		
		return 0;
		
}
