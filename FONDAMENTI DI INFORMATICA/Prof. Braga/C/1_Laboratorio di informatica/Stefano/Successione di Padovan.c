#include <stdio.h>

int main(){
	
	int i=0, num=0, max=0;
	int p[4] = {1, 1, 1, 2};
	printf("----Successione di Padovan----\n");
	printf("Fino a che numero vuoi che ti mostri i numeri della successione? ");
	scanf("%d", &num);

	for(; i<num; i++){
		if(i==0){
			printf("%d", p[0]);
		}
		else if(i==1){
			printf(",%d", p[1]);
		}
		else if(i==2){
			printf(",%d", p[2]);
		}
		else if(i>2){
			p[3]=p[1]+p[0];
			printf(",%d", p[3]);
			p[0]=p[1];
			p[1]=p[2];
			p[2]=p[3];
		}
	}
		
	return 0;
}
