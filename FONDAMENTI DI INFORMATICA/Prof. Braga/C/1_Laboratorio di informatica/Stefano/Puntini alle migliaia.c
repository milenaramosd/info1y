#include <stdio.h>
#include <string.h>
#include <math.h>

int main(){
	int n=0;
	n=12006428;
	stampa(n);
	
	return 0;
}

void stampa(int n){			//PUNTINI ALLE MIGLIAIA
	if(n<1000){
		printf("%d", n);
	}
	else{
		stampa(n/1000);
		printf(".%d%d%d", (n/100)%10, (n/10)%10, n%10);
	}
}
